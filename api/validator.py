# -*- coding: utf-8 -*-
"""
Created on Wed Sep 26 14:51:41 2018

@author: tardis
"""

import cerberus
import traceback
from datetime import datetime
from s_utils import APP_DATA, logger

cerberus.schema_registry.extend(APP_DATA['schemas_cerberus'])


class Validator(cerberus.Validator):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def _normalize_coerce_integer(self, value):
        return int(value)

    def _normalize_coerce_gender(self, value):
        if value in ['male', 'female']:
            return value
        raise ValueError(value)

    def _normalize_coerce_birthdate(self, value):
        try:
            v = datetime.strptime(value, '%d.%m.%Y')
            if v < datetime.now():
                return value
            raise ValueError(value)
        except:
            print(traceback.format_exc())
            raise ValueError(value)

    def _normalize_coerce_relatives(self, value):
        try:
            all_relatives = list()
            for citizen in self.root_document['citizens']:
                all_relatives += citizen['relatives']
                for citiz in self.root_document['citizens']:
                    if citiz['citizen_id'] in citizen['relatives'] and citizen['citizen_id'] not in citiz['relatives']:
                        raise ValueError(value)

            all_relatives = list(set(all_relatives))
            for relative in all_relatives:
                is_exist = False
                for citizen in self.root_document['citizens']:
                    if citizen['citizen_id'] == relative:
                        is_exist = True
                if not is_exist:
                    raise ValueError(value)
            return value
        except:
            logger.error(traceback.format_exc())
            raise ValueError(value)
