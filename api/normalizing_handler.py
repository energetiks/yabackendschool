# -*- coding: utf-8 -*-
import sys
import json
import traceback
from base64 import b64decode
import re
from tornado import web

from s_utils import logger
# from handler_base import HandlerBase
from validator import Validator


def normalized_body(schema):
    """
    Decorator for any Tornado handler method that requires body scheme. Works
    both with sync and async Tornado methods.
    Usage:
        from normalizing_handler import NormalizingHandler, normalized_body
        class MyHandler(NormalizingHandler):
            @normalized_body("schema")
            def post(self):
                logger.info(self.normalized_body)

    `schema` must be a valid cerberus schema or a name defined in
    cerberus.schema_registry, populated from
    s_utils.APP_DATA["schemas_cerberus"]) when you import validator.

    Instead of using this decorator you can also just write
        from normalizing_handler import NormalizingHandler
        class MyHandler(NormalizingHandler):
            def post(self):
                self._body_schema = schema
                logger.info(self.normalized_body)
    """

    def decorator(f):
        def wrapper(self, *args):
            self._body_schema = schema
            return f(self, *args)
        return wrapper
    return decorator


class NormalizingHandler(web.RequestHandler):
    """
    NormalizingHandler normalizes request body.
    """

    def normalize_body(self, schema):
        """
        Returns True if body was normalized. Sets request_handler.normalized_body
        or writes an error message and sets code 400
        """
        try:
            data = json.loads(self.request.body.decode())
        except:
            self.write({'message': 'Request body must be JSON.'})
            self.set_status(400)
            return False

        validator = Validator(schema)

        try:
            if not validator.validate(data):
                self.write({
                    'message': 'Failed validating request body.',
                    'details': validator.errors
                })
                self.set_status(400)
                logger.debug('%.10000s\n%s' % (data, validator.errors))
                return False
        except Exception as e:
            self.write({
                'message': 'Failed validating request body.',
                'details': str(e) or e.__repr__()
            })
            self.set_status(400)
            logger.error(traceback.format_exc())
            return False

        self._normalized_body = validator.document
        return True

    @property
    def normalized_body(self):
        try:
            return self._normalized_body
        except:
            if self.normalize_body(self._body_schema):
                return self._normalized_body
        raise web.Finish()
