import pytest
import json
import os
from validator import Validator

validation_data = json.load(open("test/validation_data.json", "r"))


@pytest.mark.parametrize('input_data, expected, key', list(zip(validation_data['input_data'],
                                                               validation_data['expected'],
                                                               validation_data['validators'])))
def test_validation_bad(input_data, expected, key):
    assert Validator(key).validate(input_data) == expected
