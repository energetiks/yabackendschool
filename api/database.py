# -*- coding: utf-8 -*-
"""@package dockstring
Database abstraction layer
"""

import asyncio
import asyncpg
import json
import traceback
import time
import datetime
import numpy as np
from loggz import logger
from s_utils import config, jdumps, wait, as_future, logged_function


async def _setup_connection(c):
    await c.set_type_codec(
        typename='uuid',
        encoder=str,
        decoder=str,
        schema='pg_catalog')
    await c.set_type_codec(
        typename='json',
        encoder=jdumps,
        decoder=json.loads,
        schema='pg_catalog')
    await c.set_type_codec(
        typename='jsonb',
        encoder=jdumps,
        decoder=json.loads,
        schema='pg_catalog')

# init connection pool
_pool = wait(asyncpg.create_pool(**json.loads(config['DB_PARAMS']),
                                 **{'min_size': 1, 'max_size': 8}, init=_setup_connection))


def sub(query, d):
    """
    Substitute python-style keyword args [%(key)s] with asyncpg-style positional args [$1].
    Returns tuple to insert into the fetch*/execute function, e.g.:
        sub('INSERT INTO table (a,b) VALUES (%(a)s, %(b)s)', {'a':'val', 'b': 123})
        returns ('INSERT INTO table (a,b) VALUES ($1,$2)', 'val', 123)
    """
    q = query % {k: '$%d' % (i+1) for i, k in enumerate(d)}
    return (q, ) + tuple(d.values())


@logged_function
async def insert_import(citizens):
    row = await _pool.fetchrow("INSERT INTO imports (creation_time) VALUES (CURRENT_TIMESTAMP) RETURNING import_id")

    good_keys = ["citizen_id", "town", "street", "building", "apartment", "name",
                 "birth_date", "gender", "relatives", "import_id"]
    inserted_citizens = []
    for citizen in citizens:
        citizen['import_id'] = row['import_id']
        citizen['birth_date'] = datetime.datetime.strptime(citizen['birth_date'], '%d.%m.%Y')
        inserted = list()
        for key in good_keys:
            inserted.append(citizen[key])
        inserted_citizens.append(tuple(inserted))
    res = await _pool.executemany("INSERT INTO citizens (citizen_id, town, street, building, apartment, name, "
                                                        "birth_date, gender, relatives, import_id) "
                                  "VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10)", inserted_citizens)

    return row['import_id']


@logged_function
async def edit_citizen(citizen, citizen_id, import_id):
    query = ','.join(['%s=%%(%s)s' % (k, k) for k in citizen.keys()])
    citizen['citizen_id'] = citizen_id
    citizen['import_id'] = import_id
    changed_citizen = await _pool.fetchrow(*sub("UPDATE citizens "
                                                "SET " + query + ", last_update_time = now() "
                                                "WHERE citizen_id=%(citizen_id)s and import_id=%(import_id)s"
                                                "RETURNING * ", citizen))
    citizens = await _pool.fetch("SELECT * "
                                 "FROM citizens "
                                 "WHERE import_id = $1", citizen['import_id'])
    changed_citizens = []
    for citizen in citizens:
        if citizen['citizen_id'] != changed_citizen['citizen_id']:
            if citizen['citizen_id'] in changed_citizen['relatives'] and \
                    changed_citizen['citizen_id'] not in citizen['relatives']:
                citizen['relatives'].append(changed_citizen['citizen_id'])
                changed_citizens.append(citizen)
            if citizen['citizen_id'] not in changed_citizen['relatives'] and \
                    changed_citizen['citizen_id'] in citizen['relatives']:
                citizen['relatives'].pop(citizen['relatives'].index(changed_citizen['citizen_id']))
                changed_citizens.append(citizen)

    for citizen in changed_citizens:
        res = await _pool.fetchrow("UPDATE citizens "
                                   "SET relatives=$1, last_update_time = now() "
                                   "WHERE id=$2", citizen['relatives'], citizen['id'])
    return changed_citizen


@logged_function
async def get_citizens(import_id):
    citizens = await _pool.fetch("SELECT * "
                                 "FROM citizens "
                                 "WHERE import_id = $1", import_id)
    return citizens


@logged_function
async def get_presents(import_id):
    citizens = await _pool.fetch("SELECT r.citizen_id, "
                                        "date_part('month',c.birth_date)::int as month, "
                                        "count(*) as presents "
                                 "FROM citizens c, ( "
                                     "SELECT c.citizen_id, jsonb_array_elements(c.relatives) as relative "
                                     "FROM citizens c "
                                     "WHERE import_id = $1) r "
                                 "WHERE c.citizen_id = (r.relative->>0)::bigint and c.import_id = $1 "
                                 "GROUP BY r.citizen_id, date_part('month',c.birth_date)::int", import_id)
    months_presents = {"1": [], "2": [], "3": [], "4": [], "5": [], "6": [],
                       "7": [], "8": [], "9": [], "10": [], "11": [], "12": []}
    for month in months_presents.keys():
        for citizen in citizens:
            if str(citizen['month']) == month:
                months_presents[month].append({'citizen_id': citizen['citizen_id'],
                                              'presents': citizen['presents']})
    return months_presents


@logged_function
async def get_percentile(import_id):
    towns = await _pool.fetch("SELECT town, array_agg((current_date - birth_date::date)::float / 365::float) as ages "
                              "FROM citizens "
                              "WHERE import_id = $1 "
                              "GROUP BY town", import_id)
    age_towns = []
    for town in towns:
        age_towns.append({'town': town['town'],
                          'p50': round(np.percentile(np.array(town['ages']), 50, interpolation='linear'), 2),
                          'p75': round(np.percentile(np.array(town['ages']), 75, interpolation='linear'), 2),
                          'p99': round(np.percentile(np.array(town['ages']), 99, interpolation='linear'), 2)})
    return age_towns

@logged_function
async def delete_import(import_id):
    row = await _pool.fetchrow("DELETE FROM imports WHERE import_id = $1", import_id)
    row = await _pool.fetchrow("DELETE FROM citizens WHERE import_id = $1", import_id)
    return

@logged_function
async def clear_all():
    row = await _pool.fetchrow("TRUNCATE imports, citizens")
    row = await _pool.fetchrow("ALTER SEQUENCE imports_import_id_seq RESTART WITH 1;")
    row = await _pool.fetchrow("ALTER SEQUENCE citizens_id_seq RESTART WITH 1;")
    return
