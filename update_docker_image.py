import json
import os
import subprocess
import requests
from flask import Flask, jsonify, request, Response

app = Flask(__name__)


@app.route('/update_service')
def update_service():
    subprocess.call(["docker", "pull", "docker.io/energetiks/yabackendschool:latest"])
    subprocess.call(["docker", "container", "stop", "yabackendschool"])
    subprocess.call(["docker", "container", "rm", "yabackendschool"])
    subprocess.call(["docker", "run", "--net=host", "-d", "--name", "yabackendschool", "docker.io/energetiks/yabackendschool:latest"])
    return Response('Service was updated', status=200, mimetype='text/plain')


if __name__ == "__main__":
    subprocess.call(["docker", "pull", "docker.io/energetiks/yabackendschool:latest"])
    subprocess.call(["docker", "container", "stop", "yabackendschool"])
    subprocess.call(["docker", "container", "rm", "yabackendschool"])
    subprocess.call(["docker", "run", "--net=host", "-d", "--name", "yabackendschool",
                     "docker.io/energetiks/yabackendschool:latest"])
    app.run(host='0.0.0.0', port=8181, threaded=True)

