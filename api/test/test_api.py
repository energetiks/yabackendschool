import pytest
from conftest import generate_url
import json
import os
post_data = json.load(open("test/post_data.json", "r"))

IMPORT_ID = None


@pytest.mark.gen_test
def test_insert_import(http_client, base_url):
    global IMPORT_ID
    response = yield http_client.fetch(generate_url(base_url, '/imports'), method='POST',
                                       headers=None,
                                       body=json.dumps(post_data['insert_import']))
    IMPORT_ID = json.loads(response.body)['data']['import_id']
    assert response.code == 201


@pytest.mark.gen_test
def test_patch_citizen(http_client, base_url):
    global IMPORT_ID

    for data in post_data['patch_citizen']:

        response = yield http_client.fetch(generate_url(base_url, '/imports/{}/citizens/3'.format(IMPORT_ID)),
                                           method='PATCH',
                                           headers=None,
                                           body=json.dumps(data['request']))
        response_data = json.loads(response.body)['data']
        assert response.code == 200 and response_data == data['response']


@pytest.mark.gen_test
def test_get_citizens(http_client, base_url):
    global IMPORT_ID
    response = yield http_client.fetch(generate_url(base_url, '/imports/{}/citizens'.format(IMPORT_ID)), method='GET')
    response_data = json.loads(response.body)['data']

    assert response.code == 200 and sorted(response_data, key=lambda d: d['citizen_id']) == \
           sorted(post_data['get_citizens']['response'], key=lambda d: d['citizen_id'])


@pytest.mark.gen_test
def test_get_presents(http_client, base_url):
    global IMPORT_ID
    response = yield http_client.fetch(generate_url(base_url, '/imports/{}/citizens/birthdays'.format(IMPORT_ID)),
                                       method='GET')
    response_data = json.loads(response.body)['data']

    assert response.code == 200 and response_data == post_data['get_presents']['response']


@pytest.mark.gen_test
def test_get_percentile(http_client, base_url):
    global IMPORT_ID
    response = yield http_client.fetch(generate_url(base_url, '/imports/{}/towns/stat/percentile/age'.format(IMPORT_ID)),
                                       method='GET')
    response_data = json.loads(response.body)['data']

    assert response.code == 200 and sorted(response_data, key=lambda d: d['town']) == \
           sorted(post_data['get_percentile']['response'], key=lambda d: d['town'])


@pytest.mark.gen_test
def test_delete_data(http_client, base_url):
    global IMPORT_ID
    response = yield http_client.fetch(generate_url(base_url, '/imports/{}/delete'.format(IMPORT_ID)), method='DELETE')
    assert response.code == 200
