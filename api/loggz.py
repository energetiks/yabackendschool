import logging
import logging.handlers
import logging.config
import pythonjsonlogger.jsonlogger

LOGGING_DICT = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'default': {
            'format': '%(message)s'
        }
    },
    'handlers': {
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'default'
        }
    },
    'loggers': {
        'main': {
            'handlers': ['console'],
            'level': 'DEBUG',
            'propagate': False
        },
        'werkzeug': {
            'handlers': ['console'],
            'level': 'DEBUG',
            'propagate': False
        },
        'tornado.access': {
            'handlers': ['console'],
            'level': 'DEBUG',
            'propagate': False
        },
        'tornado.application': {
            'handlers': ['console'],
            'level': 'DEBUG',
            'propagate': False
        },
        'tornado.general': {
            'handlers': ['console'],
            'level': 'DEBUG',
            'propagate': False
        }
    }
}

logging.config.dictConfig(LOGGING_DICT)

logging.getLogger('requests').setLevel(level=logging.ERROR)
logging.getLogger('matplotlib').setLevel(level=logging.WARNING)
logging.getLogger('urllib3').setLevel(level=logging.ERROR)

logger = logging.getLogger('main')
