import json
import traceback
import uuid
from tornado.httpclient import AsyncHTTPClient
from normalizing_handler import NormalizingHandler
# from handler_base import HandlerBase
from normalizing_handler import NormalizingHandler, normalized_body
from s_utils import config, logger, as_future, jdumps
import database as db
import datetime

insert_import = as_future(db.insert_import)
edit_citizen = as_future(db.edit_citizen)
get_citizens = as_future(db.get_citizens)
get_presents = as_future(db.get_presents)
get_percentile = as_future(db.get_percentile)
delete_import = as_future(db.delete_import)
clear_all = as_future(db.clear_all)


def format_citizens(citizens):
    format_citizens = []
    for citizen in citizens:
        format_citizen = {}
        for key, value in citizen.items():
            if key not in ['id', 'import_id', 'creation_time', 'last_update_time']:
                format_citizen[key] = value
            if key == 'birth_date':
                format_citizen[key] = datetime.datetime.strftime(value, '%d.%m.%Y')
        format_citizens.append(format_citizen)
    return format_citizens


class ImportsHandler(NormalizingHandler):

    @normalized_body('imports')
    async def post(self):
        import_id = await insert_import(self.normalized_body['citizens'])

        self.set_status(201)
        self.write(jdumps({"data": {"import_id": import_id}}))
        return

    @normalized_body('patch_citizen')
    async def patch(self, import_id, citizen_id):
        try:
            import_id = int(import_id)
            citizen_id = int(citizen_id)
        except:
            self.set_status(400)
            self.write("Bad request")
            return
        citizen = await edit_citizen(self.normalized_body, citizen_id, import_id)
        self.write(jdumps({'data': format_citizens([citizen])[0]}))
        self.set_status(200)
        return

    async def get(self, import_id):
        try:
            import_id = int(import_id)
        except:
            self.set_status(400)
            self.write("Bad request")
            return
        citizens = await get_citizens(import_id)
        self.write(jdumps({'data': format_citizens(citizens)}))
        self.set_status(200)
        return

    async def delete(self, import_id):
        try:
            import_id = int(import_id)
        except:
            self.set_status(400)
            self.write("Bad request")
            return
        res = await delete_import(import_id)
        self.set_status(200)
        return

class BirthdaysHandler(NormalizingHandler):
    async def get(self, import_id):
        try:
            import_id = int(import_id)
        except:
            self.set_status(400)
            self.write("Bad request")
            return
        presents = await get_presents(import_id)
        self.write(jdumps({'data': presents}))
        self.set_status(200)
        return


class PercentileHandler(NormalizingHandler):
    async def get(self, import_id):
        try:
            import_id = int(import_id)
        except:
            self.set_status(400)
            self.write("Bad request")
            return
        percentile = await get_percentile(import_id)
        self.write(jdumps({'data': percentile}))
        self.set_status(200)
        return


class ClearAllHandler(NormalizingHandler):
    async def get(self):
        res = await clear_all()
        self.set_status(200)
        self.write("All tables were cleared")
        return
