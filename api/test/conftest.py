import app as server
import json
import yaml
import pytest
import os
from validator import Validator
# import urllib.parse
# from cerberus import Validator


# https://github.com/eugeniy/pytest-tornado/issues/16#issuecomment-317677932
@pytest.fixture()
def io_loop():
    from tornado.platform.asyncio import AsyncIOMainLoop
    return AsyncIOMainLoop()


@pytest.fixture(scope='session')
def app():
    return server.make_test_app()


class ValidatorTester:
    def __init__(self):
        with open(os.path.dirname(__file__) + "/../../app_data.yaml") as schemas:
            self._schemas = yaml.load(schemas)['schemas_cerberus']
        self._validator = Validator()

    def validate(self, body, schema):
        # data = json.loads(body

        if isinstance(body, list):
            for x in body:
                print(self._schemas[schema])
                return self._validator.validate(x, self._schemas[schema])
        else:
            print(self._schemas[schema])
            return self._validator.validate(data, self._schemas[schema])


@pytest.fixture(scope='module')
def validator():
    validator = ValidatorTester()
    return validator


class DataTester:
    def __init__(self):
        with open('tests/post_data.json') as data:
            self._data = json.load(data)

    def get(self, name):
        return json.dumps(self._data[name])


@pytest.fixture(scope='module')
def data():
    data_tester = DataTester()
    return data_tester


def generate_url(base, path, **named_arguments):
    if named_arguments:
        return base + path + "?" + "&".join(map(str, ["{}={}".format(k, v) for k, v in named_arguments.items()]))
    return base + path
