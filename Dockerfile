FROM python:3.6-slim

COPY requirements.txt /server/requirements.txt
WORKDIR /server

RUN pip3 install -r requirements.txt

COPY . /server
WORKDIR /server/api

CMD python3 app.py