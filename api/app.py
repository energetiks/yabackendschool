import traceback
import tornado.ioloop
import tornado.httpserver

from s_utils import config, app_settings, logger
# from handler_base import HandlerBase
from handlers import *


router = [
    (r"/clearall", ClearAllHandler),
    (r"/imports", ImportsHandler),
    (r"/imports/(.*)/delete", ImportsHandler),
    (r"/imports/(.*)/citizens/birthdays", BirthdaysHandler),
    (r"/imports/(.*)/towns/stat/percentile/age", PercentileHandler),
    (r"/imports/(.*)/citizens/(.*)", ImportsHandler),
    (r"/imports/(.*)/citizens", ImportsHandler),
]


def make_test_app():
    test_app = tornado.web.Application(router, **app_settings)
    return test_app


if __name__ == '__main__':
    try:
        app = tornado.web.Application(router, **app_settings)

        logger.info('Serving http on port {}...'.format(config['main_server_port']))

        app.listen(config['main_server_port'])

        tornado.ioloop.IOLoop.current().start()
    except Exception:
        logger.error(traceback.format_exc())

    logger.info('Exiting API')
