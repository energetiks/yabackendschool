CREATE USER entrant;
ALTER USER entrant WITH SUPERUSER;
ALTER USER entrant WITH LOGIN;
CREATE SEQUENCE public.citizens_id_seq
	INCREMENT BY 1
	MINVALUE 1
	MAXVALUE 9223372036854775807
	START 1
	CACHE 1
	NO CYCLE;
CREATE SEQUENCE public.imports_import_id_seq
	INCREMENT BY 1
	MINVALUE 1
	MAXVALUE 9223372036854775807
	START 1
	CACHE 1
	NO CYCLE;

CREATE TABLE public.citizens (
	id serial NOT NULL,
	citizen_id int8 NOT NULL,
	town varchar(256) NOT NULL,
	street varchar(256) NOT NULL,
	building varchar(256) NOT NULL,
	apartment int8 NOT NULL,
	"name" varchar(256) NOT NULL,
	birth_date timestamp NOT NULL,
	gender varchar NOT NULL,
	relatives jsonb NOT NULL DEFAULT '[]'::jsonb,
	creation_time timestamp NOT NULL DEFAULT now(),
	last_update_time timestamp NOT NULL DEFAULT now(),
	import_id int8 NOT NULL
);
CREATE UNIQUE INDEX citizens_id_idx ON public.citizens USING btree (id);

CREATE TABLE public.imports (
	import_id bigserial NOT NULL,
	creation_time timestamp NOT NULL DEFAULT now()
);
CREATE UNIQUE INDEX imports_import_id_idx ON public.imports USING btree (import_id);
