# -*- coding: utf-8 -*-
import json
import yaml
import traceback
import signal
import tornado
import os
from functools import wraps, partial
import asyncio
import re
import subprocess
from tornado.web import HTTPError
from dotenv import load_dotenv
from loggz import logger

load_dotenv(dotenv_path=os.path.dirname(__file__) + '/../.env')
to_load_from_env = ['DB_PARAMS', 'main_server_port']
config = {x: os.getenv(x) for x in to_load_from_env}


app_settings = {
    'compress_response': False,
    'xheaders': True
}


with open(os.path.dirname(__file__) + "/../app_data.yaml") as file:
    APP_DATA = yaml.load(file)


def logged_function(f):
    """
    logged_function(f) -> None\n
    Decorator, logs call and exit of a function.
    """

    @wraps(f)
    def wrapper(*args, **kwargs):
        try:
            return f(*args, **kwargs)
        except:
            logger.error(traceback.format_exc())
            return None

    return wrapper


def jdumps(o, indent=2):
    return json.dumps(o, ensure_ascii=0, indent=indent, default=str)


def default(x, default):
    if x is None:
        return default
    else:
        return x


def get_if_exist(checked_dict, key, default_value):
    try:
        if key in checked_dict:
            return checked_dict[key]
        else:
            return default_value
    except:
        logger.info(traceback.format_exc())
        return default_value


def toTuple(x):
    if type(x) is tuple:
        return x
    else:
        return x,


def getIfDefined(d, *keys):
    ptr = d
    for key in keys:
        if type(ptr) is list:
            if key < len(ptr):
                ptr = ptr[key]
                continue
            else:
                return None
        if hasattr(ptr, '__iter__') and key in ptr:
            ptr = ptr[key]
        else:
            return None
    return ptr


def wait(f):
    try:
        loop = asyncio.get_event_loop()
        return loop.run_until_complete(f)
    except:
        return f  # workaround "the loop is already running" for spyder


def as_future(fn):
    def wrapper(*args, **kwargs):
        return asyncio.ensure_future(fn(*args, **kwargs))

    return wrapper


# Setup a hook to kill tornado gracefully
def stop_tornado(signum=None, frame=None):
    ioloop = tornado.ioloop.IOLoop.current()
    ioloop.add_callback(ioloop.stop)
    logger.info("Gracefully killed (sig %s)" % signum)


signal.signal(signal.SIGINT, stop_tornado)
signal.signal(signal.SIGTERM, stop_tornado)